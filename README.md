# 문서 협업 툴 웹 어플리케이션

> NOTE

> - MERN stack(React, Express, MongoDB, Node.js)과 Socket.io를 활용해 문서 협업 툴을 구현하였습니다.
> - 프로젝트는 frontend와 backend로 나누어 구현하였습니다.
> - [Frontend](https://gitlab.com/kde.ryan31/document-collaboration-app/-/tree/master/frontend)
> - [Backend](https://gitlab.com/kde.ryan31/document-collaboration-app/-/tree/master/backend)
> - Test Code : 미작성

# 문서 협업 툴 웹 어플리케이션 : 구현 미완료
---
- [x] 구글을 이용한 소셜 로그인이 되어야 합니다. (Firebase Auth 권장)
- [x] 로그인한 사용자는 새로운 문서를 생성할 수 있습니다.
- [x] 생성된 문서를 저장할 수 있는 기능이 있습니다. - _20초마다 자동 저장하는 기능이 있습니다._
- [x] 저장된 문서를 고유한 URL이 부여됩니다.
- [x] 문서 URL을 공유하여 다른 사용자들과 협업할 수 있습니다. - _공유하여 접근 가능하지만 문서 작업 기능에 문제 있는 상황입니다._
- [x] 로그인하지 않은 사용자는 문서 URL에 접근할 수 없습니다.
- [x] 로그인한 사용자는 문서 URL에 접근하여 문서 작업이 가능합니다. - _접근은 가능하지만 문서 작업에 문제가 있습니다._
- [x] 문서 작업은 단순 텍스트 입력만 가능하며, 그외에 텍스트 스타일(굵기, 크기, 폰트)나 기타 복잡한 문서 작업 기능은 지원하지 않습니다.
- [ ] 동시에 여러 사용자가 접속하여 동일한 문서에 작업하는 경우, 다른 동시 접속자들의 문서 작업 커서 위치가 사용자에게 표시되어야 합니다. (마우스 커서 위치가 아닌, 테스트 타이핑 작업을 하는 커서 위치입니다.)
- [x] 저장된 문서는 매 20초마다 자동 저장 되어야 하고, 다시 접속하여 작업할 경우 마지막으로 저장된 작업 내용을 보여주어야 합니다.
- [x] 로그인한 사용자는 본인이 생성한 문서 목록을 볼 수 있습니다.

# 문서 협업 툴 웹 어플리케이션 Description
### Project directory tree
```
.
│  .gitignore
│  package-lock.json
│  package.json
│  README.md
│  text.txt
│
├─backend
│  │  .env
│  │  .gitignore
│  │  app.js
│  │  package-lock.json
│  │  package.json
│  │  README.md
│  │
│  ├─bin
│  │      www
│  │
│  ├─config
│  │      mongo.connection.js
│  │      socket.js
│  │
│  ├─controllers
│  │      documentController.js
│  │      userControllers.js
│  │
│  ├─models
│  │      Document.js
│  │      User.js
│  │
│  ├─routes
│  │      documentRouter.js
│  │      userRouter.js
│  │
│  └─service
│          firebase.json
│          firebaseAdmin.js
│
└─frontend
    │  .env
    │  .gitignore
    │  package-lock.json
    │  package.json
    │  README.md
    │
    ├─public
    │      favicon.ico
    │      index.html
    │
    └─src
        │  index.js
        │
        ├─app
        │      App.js
        │
        ├─assets
        │      google_docs_logo.png
        │      google_doc_logo.png
        │      google_login_btn.png
        │      google_logo.png
        │
        ├─components
        │  ├─Documents
        │  │  ├─Document
        │  │  │      Document.js
        │  │  │
        │  │  └─MyDocument
        │  │          MyDocument.js
        │  │          MyDocumentHeader.js
        │  │
        │  ├─Error
        │  │      Error.js
        │  │      NotFound.js
        │  │
        │  ├─Login
        │  │      Login.js
        │  │
        │  └─shared
        │          GlobalStyles.js
        │
        └─service
                auth.js
                firebase.js
```

## devDependencies
- concurrently : Frontend와 Backend를 동시에 실행시키기 위해 선택했습니다.

## Getting Started
#### Clone the repository
```
https://gitlab.com/kde.ryan31/document-collaboration-app
```
#### Installation
```
npm install
```
#### Start
```
npm start
```
