const { firebaseAdminAuth } = require("../service/firebaseAdmin");
const User = require("../models/User");

exports.postSaveUser = async (req, res, next) => {
  try {
    if (!req.body.token) {
      res.status(400).json({ message: "Not Token"});
    }

    const token = await firebaseAdminAuth.verifyIdToken(req.body.token);

    if (!token.uid) {
      res.status(400).json({ message: "Token false"});
    }

    const { uid, email } = token;

    const user = await User.findOne({ uid: uid });

    if (!user) {
      await User.create({
        uid,
        email,
        documents: [],
      });
    }

    res.json({ email, uid });
  } catch (error) {
    res.status(500).json({ error });
  }
};
