const Document = require("../models/Document");
const User = require("../models/User");

exports.getAllDocs = async (req, res, next) => {
  try {
    const user = await User.findOne({ uid: req.query.id }).lean();
    const docs = await Document.find({ creator: user._id }).lean();

    res.json({ docs });
  } catch (error) {
    res.status(500).json({ error });
  }
};

exports.postCreateDoc = async (req ,res ,next) => {
  try {
    const user = await User.findOne({ email: req.body.email }).lean();

    const newDoc = await Document.create({
      creator: user._id,
      title: "제목 없는 문서",
      contents: "",
    });

    if (newDoc) {
      await User.findOneAndUpdate(
        { email: req.body.email },
        { $push: { documents: newDoc._id }},
      );
    }

    res.status(201).json({ message: 'create document' });
  } catch (error) {
    res.status(500).json({ error });
  }
};

exports.getDoc = async (req, res, next) => {
  try {
    const document = await Document.findOne({ _id: req.params.id }).lean();

    res.json({ document });
  } catch (error) {
    res.status(500).json({ error });
  }
};

exports.deleteDoc = async (req, res, next) => {
  try {
    const deleteDocument = await Document.findOneAndDelete(
      { _id: req.params.id },
      { new: true },
    );

    await User.findOneAndUpdate(
      { _id: deleteDocument.creator },
      { $pull: { documents: deleteDocument._id }},
    );

    res.json({ deleteDocument });
  } catch (error) {
    res.status(500).json({ error });
  }
};
