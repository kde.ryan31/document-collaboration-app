const Document = require("../models/Document");

const io = require("socket.io")(5001, {
  cors: {
    origin: "http://localhost:3000",
    methods: ['GET', 'POST'],
  }
});

io.on("connection", (socket) => {
  socket.on("get-document", async (idList) => {
    const document = await Document.findOne({ _id: idList.objectId });

    socket.join(idList.objectId);

    const info = {
      title: document.title,
      contents: document.contents,
    }

    socket.emit("load-document", info);

    socket.on("send-changes", (data) => {
      socket.to(idList.objectId).emit("receive-changes", data);
    });

    socket.on("save-document", async (data) => {
      const { title, contents } = data.docs;

      if (data) {
        await Document.findOneAndUpdate(
          { _id: data.id },
          {
            title: title,
            contents: contents,
          }
        );
      }
    });
  });
});

module.exports = io;
