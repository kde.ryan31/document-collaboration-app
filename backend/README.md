## 문서 협업 툴 웹 어플리케이션 : Backend
#### bin/www
- 서버를 실행하고 소켓을 연결하는 부분입니다.

#### config
- mongo.connection : MongoDB Cluster를 연결하는 부분입니다. 
- socket : 소켓을 실행하고 frontend의 요청을 받고 보내는 부분입니다.

#### controllers
- documentController : MongoDB에 저장된 docs schema에서 조회, 삭제, 수정, 생성를 하는 부분입니다.
- userController : firebase-admin 을 사용해 frontend에서 요청한 토큰의 유효성을 판별하고 유저를 생성하는 부분입니다.

#### models
- Document : docs schema를 등록하는 부분입니다.
- User : users schema를 등록하는 부분입니다.

#### routes
- documentRouter : document와 관련된 엔드 포인트을 정의한 부분입니다.
- userRouter : user와 관련된 엔드 포인트를 정의한 부분입니다.

#### service
- firebase.json : firebase의 private key 입니다. ignore 처리되었습니다.
- firebaseAdmin : firebase.json을 사용해 firebase를 초기화하는 부분입니다.

#### app.js
- 각종 미들웨어의 처리가 되는 부분입니다.

#### Dependencies
- express : 웹 서버 구현을 위해 선택했습니다.
- mongoose : mongoDB를 node.js에서 사용하기 위해 선택했습니다.
- dotenv : 환경변수를 사용하기 위해 선택했습니다.
- cors : cors 관련 에러를 처리하기 위해 선택했습니다.
- firebase-admin : firebase 구글 로그인 인증과 관련되어 사용하게 되었습니다.
- socket.io : 실시간 통신을 위해 선택했습니다.

#### devDependencies
- nodemon : 서버를 자동으로 재시작하기 위해 선택했습니다.

#### env
```
MONGO_URL=mongodb+srv://kimdaeun:kim103522@react-express.ciuhu.mongodb.net/docsDB?retryWrites=true&w=majority
````

#### firebase.json
```
// /service/ 에 위치합니다.
{
  "type": "service_account",
  "project_id": "document-collaboration-app",
  "private_key_id": "0cfe39ec1e5c12933b373f17be52899454c82ac8",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDisvGn63jymDX3\n5MzdP4dufERO3c9R8HqjU3wDRGBpYz2b7p0vZul4lZUb/CTN6q+u0zYMZpwjXCwY\nM0YIbNM2TGfG5X7E3adnRD5DqtlgQ6z2u/kk5JknmaLO2FdPlGhEUSGvYz9MiqXV\ngD7eKnkiGw8FSKLkxUw64bAkPhGzb3PqOozcsL2AAuG+WtT9fSe3ZEuOBsC0N8nQ\n4CdOQmgErfca6hLmIDig0JT7O/J0oWCZNPClzRhKpTtHnxPTHWs0Qh356RGYJqGS\nIJo72+vy+8qVhU6i4Z+anSgo3ygcksfUfsFzITIVSrKhLPJBp5c08U2hZZe+YovG\n7395oyTnAgMBAAECggEAElpS+WQ1K3aqeoIvGlj9Tt2H7ivuOn+nKLfRL2ExaPbG\n0xaTP8uAv+lkOUVwJAI0QjQJM8x5fDIHGS0gJf3MGkuUjzlJyl2jeCxshK/n30Tl\n/iejo5qk0jGRnlX3Y5Lr316uGnPaI7PfJGoN15Cas+nQN9OfPw7AqCJYpJEof7KG\n2FuRU+yVJgCZzBBkRPxI1akTePlqKIyoBuefGFdYm4aqPHp5fzGDr5eW5awqbp0I\nBwT4Ws07/gmC133Vlz1S3H1ny9OZhM7kxnJ7D1KvoT8mb8o45QX7cWV/bDVPvyZB\nqOK44BwXJzRPAEDsJLcu2Wx/BQ/rJZWfQtfJkhbZuQKBgQD1KrQ+asBF7hjM5rCo\n8fwxMqfyor+Xd6oxDOfYFxhDHV9eT0XY3jCii/arRRwk7uoerNWTKvmZ311NIvnk\nIVPTApc7gWhrEOyrnp67z0U+NsRLaH3KuKw73I/62bR4cr7FK/1y73ZUve5zsQrZ\nORd0GlN7IBVYBLd5kJHAp3hnFQKBgQDst1WWu4JfWi5p3Avg/12/uZL5beWf7xqv\nWgucDD+yzDm3SGHqd1hkSDE9CwquI1wAWUYIItJD7TX4jIs1JKjT430533tTiLwD\n8YRF4d167k+rXCd44siw/Sps5re5Vubej2WCjWZJyy/gue+LY1aYc5sEuQuouc7d\nPkbjEBmbCwKBgQD1ICtzNNzljnfxdr4vTTNwJUvSx3J0iUr7HAI7BmxVvmp6KxyV\nByLtgq+w8rp/GvmujfRE3vUNKk1pMI1V0RS0p6R59JtoYVWhCkl/Um/TEoMVOnZ7\nWEWs3IyArWdJ/sBK9/7DCjXv6K8fsWY9Zl3S0feh5aQxixW/UBLK4yb5OQKBgFCl\nxYstJyXFW7rFNWbamd4QI3rHbkwm8jh7vSjVI36PBbrj9jgjoQHJdr0NXf4mHdog\nkDOFaY2q65BakbLxYnOONcPZ1/KFy0isqBwG76ZKOupu4yPUX/usxR+FG2MRsGZz\nBGqOMiNCDnXJ40rh9UbLXC0nvkklxMyrwwrpGVAxAoGADGYGaJwYertbx8V3CP4s\nxqTsyZWQMBk56LqANZfUgehnkbZa0Zy1pI1FbsLQSWY7I9gXU10nb1Eiaf8zftbw\nvddRzT9Rm5sLCra8lxCEPbGr13/LcfGb8Q9SPLPzAft47UNxJp5VWjabuzf2FFmk\nr0hvwLPYl6MQJC6BoIWLolE=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-fl66m@document-collaboration-app.iam.gserviceaccount.com",
  "client_id": "113307967060684671058",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-fl66m%40document-collaboration-app.iam.gserviceaccount.com"
}
```
