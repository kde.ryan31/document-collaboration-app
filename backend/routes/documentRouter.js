const express = require('express');
const router = express.Router();

const {
  postCreateDoc,
  getAllDocs,
  getDoc,
  deleteDoc,
} = require('../controllers/documentController');

router.get('/', getAllDocs);
router.post('/', postCreateDoc);

router.get('/:id', getDoc);
router.delete('/:id', deleteDoc);

module.exports = router;
