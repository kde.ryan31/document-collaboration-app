const express = require('express');
const router = express.Router();

const { postSaveUser } = require('../controllers/userControllers');

router.post('/token', postSaveUser);

module.exports = router;
