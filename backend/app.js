require("dotenv").config();
require("./config/mongo.connection");

const express = require('express');
const app = express();
const cors = require('cors');

const userRouter = require('./routes/userRouter');
const documentRouter = require('./routes/documentRouter');

app.use(cors({
  origin: "http://localhost:3000",
}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/user', userRouter);
app.use('/document', documentRouter);

app.use(function(req, res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
});

module.exports = app;
