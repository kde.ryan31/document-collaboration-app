const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    uid: {
      type: String,
      required: true,
      unique: true
    },
    email: {
      type: String,
      required: true,
    },
    documents: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Document",
    }]
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("User", userSchema);
