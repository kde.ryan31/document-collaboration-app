import React, { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import styled from "styled-components";
import { io } from "socket.io-client";

import { onAuthChange } from "../../../service/auth";
import DocLogo from "../../../assets/google_doc_logo.png";

const DocumentForm = styled.form`
  height: 100%;

  .line {
    border: 1px solid rgba(145, 145, 145, 0.1);
  }

  textarea {
    width: 70%;
    height: 85%;
    margin: 40px;
    padding: 5px;
    font-size: 18px;
    font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
    line-height: 2rem;
  }
`;

const DocumentHeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px;

  .headerRightGroup {
    display: flex;
    align-items: center;

    .logoImg {
      width: 40px;
    }

    .title {
      margin-left: 10px;
      font-size: 18px;
      border-radius: 3px;
      color: rgba(41, 41, 41, 0.6);

      &:hover {
        border: 1px inset rgba(41, 41, 41, 0.1);
        border-radius: 3px;
      }

      &:focus {
        border: 2px solid rgb(16, 110, 255 );
        border-radius: 3px;
      }
    }
  }

  button {
    margin-right: 20px;
    padding: 10px 25px;
    border: 1px solid rgba(41, 41, 41, 0.3);
    border-radius: 8px;
    font-size: 14px;
    color: rgba(41, 41, 41, 0.8);
    background-color: transparent;
    cursor: pointer;
  }
  `;

export default function Document() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const { id } = useParams();
  const [socket, setSocket] = useState(false);
  const [loading, setLoading] = useState(true);
  const [docs, setDocs] = useState({
    title: "제목 없는 문서",
    contents: "",
  });
  const inetervalTime = 1000 * 20;

  const moveMyDocumentHandler = (userInfo) => {
    navigate("/mydocument", {state: {
      id: userInfo.id,
      email: userInfo.email,
    }});
  };

  const changeInputHandler  = (event) => {
    const {name, value} = event.target;

    setDocs({
      ...docs,
      [name]: value,
    });
  };

  const { title, contents } = docs;

  useEffect(() => {
    const socketAddress= io("http://localhost:5000/");
    setSocket(socketAddress);

    return () => {
      socketAddress.disconnect();
    };
  }, []);

  useEffect(() => {
    if (!socket) {
      return;
    }

    if (loading) {
      socket.once("load-document", (document) => {
        setDocs({
          title: document.title,
          contents: document.contents,
        });
        setLoading(false);
      });
    }

    onAuthChange(async (user) => {
      if (user) {
        const idList = {
          uid: user.uid,
          objectId: id,
        }
        socket.emit("get-document", idList);
      }
    });
  }, [socket, loading]);


  useEffect(() => {
    if (!socket) {
      return;
    }
    const saveDocument = setInterval(() => {
      socket.emit("save-document", { docs, id });
    }, inetervalTime);

    return () => {
      clearInterval(saveDocument);
    };
  }, [socket, docs]);

  useEffect(() => {
    if (!socket) {
      return;
    }
    socket.emit("send-changes", docs);
  }, [socket, docs]);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on("receive-changes", (docs) => {
      setDocs({
        title: docs.title,
        contents: docs.contents,
      });
    });

    return () => {
      socket.off("receive-changes");
    };
  }, [socket, docs]);

  useEffect(() => {
    onAuthChange(user => {
      !user && navigate("/");
    });
  });

  return (
    <DocumentForm>
      <DocumentHeaderContainer>
        <div className="headerRightGroup">
          <img className="logoImg" src={DocLogo} alt="logo" onClick={() => {
            moveMyDocumentHandler(state);
          }}/>
          <input
            className="title"
            name="title"
            value={title}
            onChange={changeInputHandler}
          />
        </div>
      </DocumentHeaderContainer>
      <div className="line"></div>
      <textarea
        name="contents"
        value={contents}
        onChange={changeInputHandler}
      ></textarea>
    </DocumentForm>
  );
};
