import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

import { logout, onAuthChange } from "../../../service/auth";
import { GoPlus, GoTrashcan } from "react-icons/go";
import { FcViewDetails } from "react-icons/fc";

import styled from "styled-components";
import MyDocumentHeader from "./MyDocumentHeader";
import ErrorTemplate from "../../Error/Error";

const MyDocumentBody = styled.div`
  position: relative;
  height: 100%;
`;

const MyDocumentContainer = styled.article`
  width: 100%;
  height: 100%;
  text-align: left;
  background-color: transparent;

  .line {
    border: 1px solid rgba(145, 145, 145, 0.1);
  }

  ul {
    display: grid;
    margin: 30px;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    grid-auto-rows: 300px;
    column-gap: 40px;
    row-gap: 40px;

    li {
      position: relative;
      width: 200px;
      height: 300px;
      border: 1px solid rgba(41, 41, 41, 0.2);
      border-radius: 3px;
      cursor: pointer;

      .deleteBtn {
        position: absolute;
        bottom: 2px;
        right: 5px;
        background-color: transparent;

        .deleteIcon {
          font-size: 24px;
          color: rgba(190, 1, 1, 0.7);;
        }
      }

      .content {
        height: 220px;
        padding: 5px;
        overflow: hidden;

        span {
          display: -webkit-box;
          font-size: 10px;
          word-break: break-all;
        }
      }

      .bottomGroup {
        padding: 10px;
        border-top: 1px solid rgba(41, 41, 41, 0.2);

        .title {
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;

          span{
            margin-left: 3px;
            font-size: 16px;
            font-weight: 400;
            color: rgba(41, 41, 41, 0.8);
          }
        }

        .updateDate {
          display: flex;
          align-items: center;
          margin: 10px 0;

          .docIcon {
            font-size: 28px;
          }

          span {
            font-size: 14px;
            color: rgba(41, 41, 41, 0.6);
          }
        }
      }
    }
  }
`;

const CreateDocument = styled.div`
  position: absolute;
  bottom: 30px;
  right: 40px;

  button {
    width: 55px;
    height: 55px;
    border-radius: 50px;
    background-color: white;
    box-shadow: 1px 2px 5px rgba(41, 41, 41, 0.5);
    cursor: pointer;

    .plusIcon {
      font-size: 26px;
    }
  }
`;

export default function MyDocument() {
  const navigate = useNavigate();
  const { state } = useLocation();
  const [docs, setDocs] = useState(null);

  const allDocsHandler = async () => {
    try {
      const result = await axios.get("/document", {
        params: { id: state.id  }
      });

      if (result.data) {
        setDocs(result.data.docs);
      }
    } catch (error) {
      return (
        <ErrorTemplate error={error} />
      );
    }
  };

  useEffect(() => {
    allDocsHandler();
  }, [state]);

  const deleteDocHandler = (objectId) => {
    try {
      const result = axios.delete(`/document/${objectId}`);

      if (result) {
        allDocsHandler();
      }
    } catch (error) {
      return (
        <ErrorTemplate error={error} />
      );
    }
  }

  const saveDocumentHandler = async () => {
    try {
      const result = await axios.post("/document", { email: state.email });

      if (result) {
        allDocsHandler();
      }
    } catch (error) {
      return (
        <ErrorTemplate error={error} />
      );
    }
  };

  const moveDocumentHandler = (objectId) => {
    navigate(`/document/${objectId}/edit`, {state: {
      id: state.id,
      email: state.email,
    }});
  };

  const lastUpdateDate = (date) => {
    const toStringDate = new Date(date).toLocaleTimeString('ko-KR');

    return toStringDate.slice(0, -3);
  }

  useEffect(() => {
    onAuthChange(user => {
      !user && navigate("/");
    });
  });

  return (
    <MyDocumentBody>
      <MyDocumentHeader onLogout={logout}/>
      <MyDocumentContainer>
        <div className="line"></div>
        <ul>
          {docs && docs.map(item => (
            <li key={item._id}>
              <div onClick={() => {
                moveDocumentHandler(item._id);
              }}>
                <div className="content">
                  <span>{item.contents}</span>
                </div>
                <div className="bottomGroup">
                  <div className="title">
                    <span>{item.title}</span>
                  </div>
                  <div className="updateDate">
                    <FcViewDetails className="docIcon"/>
                    <span>{lastUpdateDate(item.updatedAt)}</span>
                  </div>
                </div>
              </div>
              <button
                className="deleteBtn"
                onClick={() => {
                  deleteDocHandler(item._id);
                }}
              >
                <GoTrashcan className="deleteIcon" />
              </button>
            </li>
          ))}
        </ul>
      </MyDocumentContainer>
      <CreateDocument>
        <button onClick={() => {
          saveDocumentHandler()
        }}>
          <GoPlus className="plusIcon" />
        </button>
      </CreateDocument>
    </MyDocumentBody>
  );
};
