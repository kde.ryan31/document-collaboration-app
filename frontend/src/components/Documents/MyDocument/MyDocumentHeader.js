import React from "react";
import styled from "styled-components";

import DocsLogo from "../../../assets/google_docs_logo.png";

const MyDocumentHeaderContainer = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;

  .headerRightGroup {
    display: flex;
    align-items: center;

    .logoImg {
      width: 80px;
    }

    span {
      font-size: 24px;
      color: rgba(41, 41, 41, 0.6);
    }
  }

  .logoutBtn {
    margin-right: 20px;
    padding: 10px 25px;
    border: 1px solid rgba(41, 41, 41, 0.3);
    border-radius: 8px;
    font-size: 14px;
    color: rgba(41, 41, 41, 0.8);
    background-color: transparent;
    cursor: pointer;
  }
`;

export default function MyDocumentHeader({ onLogout }) {
  return (
    <MyDocumentHeaderContainer>
      <div className="headerRightGroup">
        <img
          className="logoImg"
          src={DocsLogo}
          alt="logo"
        />
        <span>문서</span>
      </div>
      <div>
        <button
          className="logoutBtn"
          onClick={onLogout}
        >
          로그아웃
        </button>
      </div>
    </MyDocumentHeaderContainer>
  );
};
