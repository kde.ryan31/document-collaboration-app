import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { login, onAuthChange } from "../../service/auth";

import styled from "styled-components";
import Logo from "../../assets/google_logo.png";
import LoginBtn from "../../assets/google_login_btn.png";

const LoginContainer = styled.article`
  transform: translateY(400px);
  width: 450px;
  height: 320px;
  border: 1px solid rgba(41, 41, 41, 0.2);
  border-radius: 10px;
  box-shadow: 1px 1px 5px rgba(214, 210, 210, 0.1);

  .logoImg {
    width: 100px;
    margin-top: 20px;
  }

  .loginText {
    margin: 10px 0 20px 0;
    font-size: 24px;
  }

  .googleLoginText {
    margin-bottom: 50px;
    font-size: 16px;
    font-family: Arial, Helvetica, sans-serif;
  }

  .loginBtn {
    background-color: transparent;
    cursor: pointer;
  }
`;

const LoginBtnImg = styled.img.attrs({
  src: `${LoginBtn}`
})`
background-color: white;
`;

export default function Login() {
  const navigate = useNavigate();

  const onLogin = async (event) => {
    const result = await login(event.target.id);

    moveMyDocument(result.data);
  };

  const moveMyDocument = (userInfo) => {
    navigate("/mydocument", {state: {
      id: userInfo.uid,
      email: userInfo.email,
    }});
  };

  useEffect(() => {
    onAuthChange(user => {
      user && moveMyDocument({
        id: user.uid,
        email: user.email,
      });
    });
  });

  return (
    <LoginContainer>
      <div>
        <img
          className="logoImg"
          src={Logo}
          alt="logo"
        />
      </div>
      <p className="loginText">로그인</p>
      <p className="googleLoginText">Google 계정 사용</p>
      <button
        className="loginBtn"
        onClick={onLogin}
      >
        <LoginBtnImg id="Google" />
      </button>
    </LoginContainer>
  );
};
