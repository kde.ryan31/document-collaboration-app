import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import App from "./app/App";
import GlobalStyles from "./components/shared/GlobalStyles";

ReactDOM.render(
  <Router>
    <GlobalStyles/>
    <App />
  </Router>,

  document.getElementById("root")
);
