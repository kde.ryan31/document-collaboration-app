import { firebaseAuth, googleProvider } from "./firebase";
import axios from "axios";

import ErrorTemplate from "../components/Error/Error";

export const login = async (name) => {
  try {
    const provider = getProvider(name);
    await firebaseAuth.signInWithPopup(provider);

    const token = await firebaseAuth.currentUser.getIdToken(true);

    if (token) {
      const result = await axios.post("http://localhost:5000/user/token", { token });
      return result;
    }
  } catch (error) {
    return (
      <ErrorTemplate error={error} />
    );
  }
};

export const logout = async () => {
  try {
    await firebaseAuth.signOut();
  } catch (error) {
    return (
      <ErrorTemplate error={error} />
    );
  }
};

export const onAuthChange = (callback) => {
  return firebaseAuth
          .onAuthStateChanged(user => callback(user));
};

const getProvider = (name) => {
  if (name === 'Google') {
    return googleProvider;
  }

  throw new Error(`${name} is unknown provider.`);
};
