import React from "react";
import { Route, Routes } from "react-router-dom";
import styled from "styled-components";

import Login from "../components/Login/Login";
import Document from "../components/Documents/Document/Document";
import MyDocument from "../components/Documents/MyDocument/MyDocument";
import NotFound from "../components/Error/NotFound";

const Main = styled.div`
  width: 100%;
  height: 100vh;
`;

export default function App() {
  return (
    <Main>
      <Routes>
        <Route path="/" element={<Login /> } />
        <Route path="/document/:id/edit" element={<Document /> } />
        <Route path="/mydocument" element={<MyDocument /> } />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Main>
  );
}
