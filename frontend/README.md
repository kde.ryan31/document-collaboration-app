# 문서 협업 툴 웹 어플리케이션 : Frontend

## src
## app
- App : 컴포넌트들의 라우트 설정을 담당합니다.

## components
#### Documents
- Document/Document : 문서 작업을 담당하는 view 입니다. 소켓과 연결되어 있고 자동 저장과 수정 기능이 있습니다.

#### MyDocument
- MyDocument : 로그인 후 자신의 작업물을 볼 수 있는 view 입니다. 문서의 생성과 삭제 기능이 있습니다.
- MyDocumentHeader : MyDocument의 Header 부분입니다. 로그아웃 기능이 있습니다.

#### Error
- Error : Error Handling을 위한 Error 템플릿 부분입니다.
- NotFound : 잘못된 경로로 갈 시 보여주는 view 입니다.

#### Login
- Login : 첫 페이지로 구글 로그인을 할 수 있는 view 입니다. 구글 로그인 기능이 있습니다.

#### service
- auth : login, logout, 로그인/로그아웃 판별 함수가 있는 인증 부분입니다.
- firebase : firebase를 초기화하는 부분입니다.

## Dependencies
- react, react-dom, react-scripts : 사용자 인터페이스 프레임워크로 react를 선택했습니다.
- react-router-dom : 페이지 이동을 처리하기 위해서 선택했습니다.
- react-icons : icon을 보다 편하게 가져오기 위해 선택했습니다.
- styled-components : 재사용성, scss의 중첩 문법을 사용하기 위해 선택했습니다.
- firebase : firebase 기반 구글 로그인을 구현하기 위해 사용하였습니다.
- axios : 서버와의 통신을 위해 선택했습니다.
- socket.io-client : 소켓 서버(backend)와의 통신을 위해 선택했습니다.

## env
```
REACT_APP_FIREBASE_API_KEY=AIzaSyBCyjjhKNf_8K9-l32yNpv4xfDowYUDZAg
REACT_APP_FIREBASE_AUTH_DOMAIN=document-collaboration-app.firebaseapp.com
REACT_APP_FIREBASE_PROJECT_ID=document-collaboration-app
```
